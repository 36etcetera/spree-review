module Spree
  module ProductsControllerDecorator
    def self.prepended(base)
      base.include ::Spree::ReviewsHelper
      base.helper_method *::Spree::ReviewsHelper.public_instance_methods
    end

    reviews_fields = [:avg_rating, :reviews_count]
    reviews_fields.each { |attrib| Spree::PermittedAttributes.product_attributes << attrib }


    Spree::Api::ApiHelpers.class_eval do
      reviews_fields.each { |attrib| class_variable_set(:@@product_attributes, class_variable_get(:@@product_attributes).push(attrib)) }
    end
    
 end
end

::Spree::ProductsController.prepend(Spree::ProductsControllerDecorator)
